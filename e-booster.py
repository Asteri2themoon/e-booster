import youtube_dl
import librosa
import numpy as np

import uuid


def download_wav(url: str, output: str = f"tmp/{uuid.uuid1()}.wav") -> str:
    ydl_opts = {
        "outtmpl": output,
        "format": "bestaudio/best",
        "postprocessors": [{"key": "FFmpegExtractAudio", "preferredcodec": "wav",}],
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        infos = ydl.extract_info(url, download=True)
        filename = ydl.prepare_filename(infos)

    return filename


def audio2earrape(
    input_file: str, output_file: str, sample_rate: int = 16000, gain: float = 20
):
    if gain < 10.0:
        raise Exception(
            "We make ear rape here, try something else if you're to weak to handle gain < 10"
        )

    data, sample_rate = librosa.load(input_file, sr=sample_rate, mono=True)
    data *= gain / np.std(data)  # advanced audio signal processing
    librosa.output.write_wav(output_file, data, sr=sample_rate)


if __name__ == "__main__":
    import argparse
    import os

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--url", "-u", help="url of the video on youtube",
    )
    parser.add_argument(
        "--tmp", "-t", default="/var/tmp", help="temporary file directory",
    )
    parser.add_argument(
        "--sample-rate",
        "-s",
        type=int,
        default=16000,
        help="sample rate of the input file",
    )
    parser.add_argument(
        "--gain", "-g", type=float, default=20.0, help="tune gain",
    )
    parser.add_argument(
        "--output", "-o", default="earrape.wav", help="output file (wav format)",
    )
    args = parser.parse_args()

    tmp_file = download_wav(
        url=args.url, output=os.path.join(args.tmp, f"{uuid.uuid1()}.wav")
    )

    audio2earrape(
        input_file=tmp_file,
        output_file=args.output,
        sample_rate=args.sample_rate,
        gain=args.gain,
    )
    os.remove(tmp_file)
